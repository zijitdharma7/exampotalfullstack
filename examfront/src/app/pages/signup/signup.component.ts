import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from 'src/app/services/user.service';
import swal from 'sweetalert2';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private userService:UserService, private _snack:MatSnackBar) { }
//Create one object 
  public user ={
    username:' ',
    password:'',
    firstname:'',
    lastname:'',
    email:'',
    phone:'',
  };

  ngOnInit(): void {
  }

  formSubmit(): void{
    console.log(this.user);
    if(this.user.username =='' || this.user.username == null)
    {
      this._snack.open('User Name is require')
      // alert('this is require field');
      return;
    }
    
    //add user service 
    this.userService.addUser(this.user).subscribe(
      (data) => {
        //success 
        console.log(data);
        // alert('Successfully');
        swal.fire('Success','User is Register','success')
      },
      (error) => {
        console.log(error);
        this._snack.open('Something went Wrong','',{
          duration:1000
        })
      }
    )

  }

}
