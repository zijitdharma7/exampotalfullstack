package com.exampotal.controller;

import com.exampotal.model.Role;
import com.exampotal.model.User;
import com.exampotal.model.UserRole;
import com.exampotal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

@RestController
@CrossOrigin(origins = "http://localhost:4200") // allowed access frontend
public class UserController {

    @Autowired
    private UserService userService;

    //Create User
    @PostMapping("/user/")
    public User createUser(@RequestBody User user) throws Exception {

    user.setProfile("default.png");
    Set<UserRole> roles = new HashSet<>();

    Role role = new Role();
    role.setRoleId(1L);
    role.setRoleName("NORMAL");

    UserRole userRole = new UserRole();
    userRole.setUser(user);
    userRole.setRole(role);

    roles.add(userRole);

    return this.userService.createUser(user,roles);

    }

    @GetMapping("/user/{username}")
    public User getUser(@PathVariable("username") String username){
        return  this.userService.getUser(username);
    }

//    delete the user
    @DeleteMapping("/user/{userId}")
    public void deleteUser(@PathVariable("userId") long userId){
        this.userService.deleteUser(userId);
    }

    //Update user --->>>>> put method
    @PutMapping("/user/{userId}")
    public User updateUser(@RequestBody User user, @PathVariable("userId") long userId){
        this.userService.updateUSer(user, userId);
        return user;
    }

}
