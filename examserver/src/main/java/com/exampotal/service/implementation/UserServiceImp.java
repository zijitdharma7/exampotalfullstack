package com.exampotal.service.implementation;

import com.exampotal.model.User;
import com.exampotal.model.UserRole;
import com.exampotal.repo.RoleRepository;
import com.exampotal.repo.UserRepository;
import com.exampotal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import java.util.List;


import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public User createUser(User user, Set<UserRole> userRoles) throws Exception {
        User local = this.userRepository.findByUsername(user.getUsername());
        if(local != null){
            System.out.println("User is Already there !! ");
            throw new Exception("User already present !!");
        }else {
            //User Create
            for (UserRole ur : userRoles) {
                roleRepository.save(ur.getRole());
            }
            user.getUserRole().addAll(userRoles);
            local = this.userRepository.save(user);
            return local;
        }
    }

    //getting user ----get User
    @Override
    public User getUser(String username) {
        return this.userRepository.findByUsername(username);
    }

    //Delete User By ID
    @Override
    public void deleteUser(Long userId) {
        this.userRepository.deleteById(userId);
    }

    @Override
    public void updateUSer(User user, long userId) {

    }
//  ------update ------
//    @Override
//    public void updateUser(User user, long userId) {
//        list.stream().map(course1 ->{
//            if(user.getId() == userId){
//                user.setUsername(user.getUsername());
//                user.setFirstName(user.getFirstName());
//                user.setLastName(user.getLastName());
//            }
//            return user;
//        }).collect(Collectors.toList());
//    }
}

