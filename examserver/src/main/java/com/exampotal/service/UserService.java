package com.exampotal.service;

import com.exampotal.model.User;
import com.exampotal.model.UserRole;

import java.util.Set;

public interface UserService {
    //For Creating User
    public User createUser( User user, Set<UserRole> userRoles) throws Exception;
    //get User
    public User getUser(String username);

    //delete user by ID
    public void deleteUser(Long userId);

    public void updateUSer(User user, long userId);

    //user update by id
//    public void updateUser(User user, long userId);

}
